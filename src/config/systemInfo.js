const name = {
  ename: 'tucent OA',
  cname: '涂讯 OA'
}
const releaseInfo = {
  year: '2019',
  month: '11',
  day: '14',
  version: '0.1'
}
const company = {
  ename: 'tucent',
  cname: '涂讯'
}

export {
  name,
  releaseInfo,
  company
}
