import { constantRouterMap, asyncRouter } from '@/config/router.config'

/**
 * 单账户多角色时，使用该方法可过滤角色不存在的菜单
 *
 * @param roles
 * @param route
 * @returns {*}
 */
// eslint-disable-next-line
function hasRole(roles, route) {
  if (route.meta && route.meta.roles) {
    return route.meta.roles.includes(roles.id)
  } else {
    return true
  }
}

/**
 * 过滤账户是否拥有某一个权限，并将菜单从加载列表移除
 *
 * @param permission
 * @param route
 * @returns {boolean}
 */
function isHasPermission (route, permissions) {
  let flag = false
  for (let i = 0, len = permissions.length; i < len; i++) {
    flag = route.path === permissions[i].url
    if (flag) {
      // 添加操作权限
      route.meta.permission = []
      if (permissions[i].actions && permissions[i].actions.length) {
        permissions[i].actions.forEach(action => {
          route.meta.permission.push(action)
        })
      }
      return true
    }
  }
  return false
}

function getFilterAsyncRouter (routerMap, permissions) {
  const accessedRouters = routerMap.filter(
    route => {
      if (isHasPermission(route, permissions)) {
        if (route.children && route.children.length) {
          route.children = getFilterAsyncRouter(route.children, permissions)
        }
        return true
      }
      return false
    }
  )
  return accessedRouters
}

const permission = {
  state: {
    routers: constantRouterMap,
    asyncRouters: []
  },
  mutations: {
    SET_ROUTERS: (state, routers) => {
      state.addRouters = routers
      state.routers = constantRouterMap.concat(routers)
    },
    SET_ASYNC_ROUTERS: (state, asyncRouters) => {
      state.asyncRouters = asyncRouters
      // state.addRouters = asyncRouters
      state.routers = constantRouterMap.concat(asyncRouters)
    }
  },
  actions: {
    ConfigRoutes ({ commit }, permissions) {
      return new Promise(resolve => {
        const accessedRouters = getFilterAsyncRouter(asyncRouter, permissions)
        console.log(accessedRouters)
        commit('SET_ASYNC_ROUTERS', accessedRouters)
        resolve()
      })
    }
  }
}

export default permission
